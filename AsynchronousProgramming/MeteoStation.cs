﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsynchronousProgramming
{
    class MeteoStation
    {
        public int Temperature{ get; set; }
        private int MeteoStationId{ get; set; }

        private static int MeteoStationIdContor = 0;

        private static List<int> PercentagePositionList;

        private static int NumberOfStations = 10;


        //constructor implicit
        //Nu e una din cele mai bune metode de lucru cu constructori dar am lucrat asa doar pentru demonstratie si pentru a simplifica codul din Metoda Main
        //Pentru fiecare statie avem un contor ce semnifica id-ul statiei respective
        //Apoi obtinem lista cu pozitiile random pentru cele 25% statii
        //Iar in final apelam metoda GetTemp pentru a afla temperatura random
        public MeteoStation()
        {
            MeteoStationIdContor++;
            this.MeteoStationId = MeteoStationIdContor;
            PercentagePositionList = Percentage();
            GetTemp();
        }

        //constructor cu parametrii
        /*public MeteoStation(int Temperature)
        {
            MeteoStationIdContor++;
            this.MeteoStationId = MeteoStationIdContor;
            this.Temperature = Temperature;
        }*/


        //metoda de afisare a informatiilor statiei
        public override string ToString()
        {
            return "Meteo Station " + MeteoStationId + " recived temperature " + Temperature;
        }


        //Am incercat si varianta in care metoda returneaza valoarea obtinuta
        //Merge cu partea de cod comentata din Metoda Main
        
        /*public  async Task<int> GetTemperature()
        {
            Random rndTemperature = new Random();
            Random rndTime = new Random();
            int time = rndTime.Next(1000, 5000);
            int result = await Task.Run(() => rndTemperature.Next(18, 32));
            Temperature = result;
            //Console.WriteLine(time);
            Task.Delay(time).Wait();
            return result;
        }*/



        //Metoda Percentage formeaza o lista cu pozititiile random a 25% dintre statiile meteo.
        //Lista este statica in cadrul clasei iar metoda este folosita in constructorul implicit pentru ca lista returnata sa fie folosita in metoda GetTemp
        //astfel pentru fiecare statie in parte se verifica indexul ei si se compara cu lista ce contine pozitiile random pentru cele 25% din statiile meteo
        //In cazul in care cele doua pozitii coincid se arunca o exceptie cu mesajul Serviciu indisponibil
        public static List<int> Percentage()
        {
            int value = (NumberOfStations * 25) / 100;
            Random rnd = new Random();
            List<int> randomPositions = new List<int>();
            for (int i = 0; i < value; i++)
            {
                int rndValue = rnd.Next(1, NumberOfStations);
                if(!(randomPositions.Contains(rndValue)))
                {
                    randomPositions.Add(rndValue);
                }    
              
            }
            return randomPositions;
        }

        //Aceasta metoda calculeaza o valoare random pentru temperatura statiei meteo si o valoare random pentru delay
        //Daca indexul statie meteo este gasit in lista cu pozitiile random atunci se arunca exceptia
        //Pentru fiecare statie se apeleaza metoda ToString pentru a afisare detaliile statiei(id+temperatura)
        public async Task GetTemp()
        {
            Random rndTemperature = new Random();
            Random rndTime = new Random();
            int time = rndTime.Next(1000, 5000);

            try
            {
                await Task.Run(() =>
                {
                    Temperature = rndTemperature.Next(18, 32);
                    //Se poate comenta aceasta instructiune pentru verificarea metodei fara aruncarea exceptiei
                    if (PercentagePositionList.Contains(MeteoStationId))
                        throw new Exception("Serviciu indisponibil");
                    Console.WriteLine(ToString());
                    Task.Delay(time).Wait();
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
          
            
        }  
    }
}
